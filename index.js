/*
  Name:         logging.js
  Author        Austin Garrod
  Date:         3/18/2017
  Description:  Created to simplify logging for use in
                Developer Data applications.
  Requirements: moment.js
*/

// Libraries
var moment = require('moment'); // For date management

// Constants
const UNSPECIFIED_MESSAGE = "Message unspecified";
const PREFIX_LOG = "LOG";
const PREFIX_INFO = "INFO";
const PREFIX_ERROR = "ERROR";
const PREFIX_WARN = "WARNING";
const MOMENT_FORMAT = "DD-MM-YYYY HH:mm:ss";

// Functions to export
module.exports = {
  log: log,
  info: info,
  error: error,
  warn: warn
};

// Functions

// log basic log event to console
function log(message) {
 var toConsole = (message ? message : UNSPECIFIED_MESSAGE);
 console.log("[" + PREFIX_LOG + "]     " + moment().format(MOMENT_FORMAT) + ": " + toConsole);
}

// log info event to console
function info(message) {
 var toConsole = (message ? message : UNSPECIFIED_MESSAGE);
 console.info("[" + PREFIX_INFO + "]    " + moment().format(MOMENT_FORMAT) + ": " + toConsole);
}

// log error event to console
function error(message) {
 var toConsole = (message ? message : UNSPECIFIED_MESSAGE);
 console.error("[" + PREFIX_ERROR + "]   " + moment().format(MOMENT_FORMAT) + ": " + toConsole);
}

// log warning event to console
function warn(message) {
 var toConsole = (message ? message : UNSPECIFIED_MESSAGE);
 console.warn("[" + PREFIX_WARN + "] " + moment().format(MOMENT_FORMAT) + ": " + toConsole);
}