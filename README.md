# Developer Data Logger #

This application was developed to do basic logging to the console with prepended level information and timestamps.

## How to set up ##

1. Run 'npm install --save devdata-logger' in console
2. add the following or an equivilant to your project;
```
#!javascript
var Logger = require('devdata-logger');

```
   
## How to use ##

```
#!javascript

// log basic log event to console
Logger.log("Your log message here");

// log info event to console
Logger.info("Your info message here");

// log error event to console
Logger.error("Your error message here");

// log warning event to console
Logger.warn("Your warning message here");

```